#!/bin/env python
#-*-coding:utf-8-*-
import os
import sys
import string
import time
import datetime
import pymysql

class MySQL:
    def __init__(self, host, port, user, passwd, timeout, charset):
        '''
        初始化MySQL对象

        Args:
            host: 数据库地址
            port: 数据库端口
            user: 数据库用户
            passwd: 数据库密码
            timeout: 超时时间
            charset: 字符集
        '''
        self.host = host
        self.port = port
        self.user = user
        self.passwd = passwd
        self.timeout = timeout
        self.charset = charset

    def db_connect(self):
        '''
        获取数据库连接

        Returns:
            返回数据库连接
        '''
        connect=pymysql.connect(host=self.host,
                                user=self.user,
                                passwd=self.passwd,
                                port=int(self.port),
                                connect_timeout=int(self.timeout),
                                charset='utf8')
        return connect

    def flush_hosts(self, cursor):
        '''
        清空MySQL缓存表

        Args:
            cursor: 数据库游标
        '''
        cursor.execute('flush hosts;')

    def get_mysql_status(self,cursor):
        '''
        获取数据库当前状态信息

        Args:
            cursor: 数据库游标
        Returns:
            返回字典形式的状态信息
        '''
        cursor.execute('show global status;')
        data_list=cursor.fetchall()
        data_dict={}
        for item in data_list:
            data_dict[item[0]] = item[1]
        return data_dict

    def get_mysql_variables(self,cursor):
        '''
        获取当前的数据库变量信息

        Args:
            cursor: 数据库游标
        Returns:
            返回字典形式的数据库变量信息
        '''
        cursor.execute('show global variables;')
        data_list=cursor.fetchall()
        data_dict={}
        for item in data_list:
            data_dict[item[0]] = item[1]
        return data_dict

    def get_mysql_version(self,cursor):
        '''
        获得数据库当前软件版本
        
        Args:
            cursor: 数据库游标
        Returns:
            数据库的版本信息
        '''
        cursor.execute('select version();')
        return cursor.fetchone()[0]
